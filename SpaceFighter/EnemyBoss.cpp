
#include "EnemyBoss.h"

//declares a new item EnemyBoss, set the speed, strength, and hit radius
EnemyBoss::EnemyBoss()
{
	SetSpeed(50);
	SetMaxHitPoints(50);
	SetCollisionRadius(70);
}

//this sets how the boss moves across the screen, as well as whether or not the boss is active, if its no longer on screen the object is deactivated
void EnemyBoss::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}

//draws up the boss object
void EnemyBoss::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}