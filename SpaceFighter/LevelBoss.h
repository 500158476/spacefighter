#pragma once

#include "Level.h"

class LevelBoss : public Level
{

public:

	LevelBoss() { }

	virtual ~LevelBoss() { }

	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void UnloadContent() { }

};