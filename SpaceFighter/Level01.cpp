

#include "Level01.h"
#include "BioEnemyShip.h"
#include "GameplayScreen.h"
#include "GameObject.h"
#include "EnemyBoss.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture *pTexture2 = pResourceManager->Load<Texture>("Textures\\BioEnemyShip2.png");
	Texture *pTexture3 = pResourceManager->Load<Texture>("Textures\\BioEnemyShip3.png");
	Texture *pBossTexture = pResourceManager->Load<Texture>("Textures\\EnemyBoss.png");

	const int COUNT = 21;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	float delay = .1; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();

		//Spawns other enemey ships with varying strengths and speeds
		switch (Math::GetRandomInt(0, 2))
		{
		case 0:
			pEnemy->SetTexture(pTexture);
			pEnemy->SetSpeed(200);
			pEnemy->SetMaxHitPoints(1);
			break;
		case 1:
			pEnemy->SetTexture(pTexture2);
			pEnemy->SetSpeed(150);
			pEnemy->SetMaxHitPoints(2);
			break;
		case 2:
			pEnemy->SetTexture(pTexture3);
			pEnemy->SetSpeed(100);
			pEnemy->SetMaxHitPoints(3);
			break;
		default:
			break;
		}
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	position.Set(0.5 * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

	//createda a new instance of the Enemy boss, set the texture as the boss skin, and it will spawn after 20 seconds
	EnemyBoss *pBoss = new EnemyBoss();
	pBoss->SetTexture(pBossTexture);
	pBoss->SetCurrentLevel(this);
	pBoss->Initialize(position, 20);
	AddGameObject(pBoss);

	double mindelays[COUNT] =
	{
		0.0, 0.25, 0.25,
		0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25,
		0.5, 0.3, 0.3, 0.3, 0.3
	};

	delay = 21;
		for (int i = 0; i < 50; i++)
		{
			int delayrand = Math::GetRandomInt(0, 20);
			int posrand = Math::GetRandomInt(200, 1400);
			delay += mindelays[delayrand];
			position.Set(posrand /** Game::GetScreenWidth()*/, -pTexture->GetCenter().Y);

			BioEnemyShip *pEnemy = new BioEnemyShip();
			switch (Math::GetRandomInt(0, 2))
			{
			case 0:
				pEnemy->SetTexture(pTexture);
				pEnemy->SetSpeed(200);
				pEnemy->SetMaxHitPoints(1);
				break;
			case 1:
				pEnemy->SetTexture(pTexture2);
				pEnemy->SetSpeed(150);
				pEnemy->SetMaxHitPoints(2);
				break;
			case 2:
				pEnemy->SetTexture(pTexture3);
				pEnemy->SetSpeed(100);
				pEnemy->SetMaxHitPoints(3);
				break;
			default:
				break;
			}
			pEnemy->SetCurrentLevel(this);
			pEnemy->Initialize(position, (float)delay);
			AddGameObject(pEnemy);
		}

	Level::LoadContent(pResourceManager);

}

